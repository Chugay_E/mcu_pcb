/**
 * @file		sh1106.h
 * @author		Chugay E.A.
 * @version		0.01
 *              -   Начальная версия.
 *              0.02
 *              -   Изменена функция send_frame_to_display(), в нее добавлена
 *                  возможность передачи аргумента функции кадра.
 *
 * @date		26.05.2019
 *
 * @brief       Функции, обеспечивающие инициализацию перефирийных модулей МК,
 *              инициализацию контроллера SH1106 и их совместную работу.
 * @details
 * File used UTF-8 code page. Don't convert other codepage for correct work.
 */

#ifndef SH1106_H_
#define SH1106_H_

/*------------------------------------------------------------------ Includes */
#include "display_config.h"
#include "frame_builder.h"

/*------------------------------------------------------- Function prototypes */
/**
 * @brief	Инициализация SPI, используемых GPIO, передача команд инициализации в дисплей.
 * @param	Нет
 * @retval	Нет
 */
void init_display (void);

/**
 * @brief	Передача команды отключения в дисплей, де-инициализация SPI и используемых GPIO.
 * @param	Нет
 * @retval	Нет
 */
void deinit_display (void);

/**
 * @brief	Настройка контрастности дисплея
 * @param	value - новое значение контрастности
 * @retval	Нет
 */
void display_set_contrast (uint8_t value);

/**
 * @brief	Функция передачи кадра в дисплей.
 * @param	signal - переменная для управления построением кадра
 * @param	void(*frame)(uint8_t) - указатель на функцию построения изображения (отрисовки кадра).
 * @retval	Нет.
 */
void send_frame_to_display (uint8_t signal, void(*frame)(uint8_t));

/**
 * @brief   Установка флага display_upd_en. Флаг сбрасывается при передаче последней строки кадра.
 * @param   Нет.
 * @retval  Нет.
 */
void display_upd_en (void);

/**
 * @brief   Запрос состояния флага display_upd_en.
 * @param   Нет.
 * @return  1 - флаг взведен, 0 - сброшен.
 */
uint8_t display_upd_get (void);

#endif	//	SH1106_H_
