/**
 * @file		screen.h
 * @author		Chugay E.A.
 * @version		0.01
 * @date		03.05.2021
 * @brief		Функции отрисовки экрана с использованием графических примитивов
 * File used UTF-8 code page. Don't convert other codepage for correct work.
 */

#ifndef SCREEN_H_
#define SCREEN_H_

/*----------------------------------------------------------------- Includes: */
#include "main.h"
#include "widget.h"

/*-------------------------------------------------------------------- Macro: */

/*------------------------------------------------------ Function prototypes: */

/*----------------------------------------------------- Верхний уровень меню: */
/**
 * @brief   Прокрутка списка основного (главного) меню.
 * @param   signal - выбранная в данный момент строка меню.
 * @retval  Нет.
 */
void top_level_menu_screen (uint8_t signal);

#endif /* SCREEN_H_ */
