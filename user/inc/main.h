/**
 * @file		main.h
 * @author		Chugay E.A.
 * @version		0.01
 * @date		03.05.2021
 * @brief		Базовый проект для платы mcu_pcb [rev1].
 * @code
 * +-----+------+------+--------------------------------------------------+
 * | PIN | PORT | GPIO |                        FUNCTION                  |
 * +-----+------+------+--------------------------------------------------+
 * | 10  | PA0  | IN   | Кнопка ВЛЕВО                                     |
 * | 11  | PA1  | IN   | Кнопка ВВЕРХ                                     |
 * | 12  | PA2  | IN   | Кнопка ВНИЗ                                      |
 * | 13  | PA3  | IN   | Кнопка ВПРАВО                                    |
 * | 14  | PA4  | OUT  | Дисплей, DC                                      |
 * | 15  | PA5  | SPI1 | Дисплей, SCK                                     |
 * | 16  | PA6  | OUT  | Дисплей, RESET                                   |
 * | 17  | PA7  | SPI1 | Дисплей, SDA                                     |
 * | 29  | PA8  |      |                                                  |
 * | 30  | PA9  |      |                                                  |
 * | 31  | PA10 | OUT  | USB, disconnect                                  |
 * | 32  | PA11 | USB  | USB, DM                                          |
 * | 33  | PA12 | USB  | USB, DP                                          |
 * | 34  | PA13 | SWD  | SWD, DIO                                         |
 * | 37  | PA14 | SWD  | SWD, CLK                                         |
 * | 38  | PA15 |      |                                                  |
 * +-----+------+------+--------------------------------------------------+
 * | 18  | PB0  | OUT  | Дисплей, CE                                      |
 * | 19  | PB1  |      |                                                  |
 * | 20  | PB2  |      |                                                  |
 * | 39  | PB3  |      |                                                  |
 * | 40  | PB4  |      |                                                  |
 * | 41  | PB5  |      |                                                  |
 * | 42  | PB6  |      |                                                  |
 * | 43  | PB7  |      |                                                  |
 * | 45  | PB8  |      |                                                  |
 * | 46  | PB9  |      |                                                  |
 * | 21  | PB10 |      |                                                  |
 * | 22  | PB11 |      |                                                  |
 * | 25  | PB12 |      |                                                  |
 * | 26  | PB13 |      |                                                  |
 * | 27  | PB14 |      |                                                  |
 * | 28  | PB15 |      |                                                  |
 * +-----+------+------+--------------------------------------------------+
 * |  2  | PC13 |      |                                                  |
 * |  3  | PC14 |      |                                                  |
 * |  4  | PC15 |      |                                                  |
 * +-----+------+------+--------------------------------------------------+
 * @endcode
 *
 */

#ifndef MAIN_H_
#define MAIN_H_

/*----------------------------------------------------------------- Includes: */
#include "stm32f1xx.h"
#include "button.h"
#include "state_machine.h"
#include "sh1106.h"
#include "screen.h"
#include "usb_device.h"

#include "file_gen.h"

/*-------------------------------------------------------------------- Macro: */
#define IWDG_RESET          (IWDG->KR = 0xAAAA) //	сброс IWDG

//  Структура статусного регистра:
typedef union {
    struct {
        volatile uint32_t btn_upd_en    :1;     //  разрешение опроса кнопок
        volatile uint32_t reserve       :31;    //  РЕЗЕРВ
    };
    volatile uint32_t value;
} STATUS_FLAG;

//  Сигналы управления:
enum Signal {
    SIGNAL_NONE = 0,
    SIGNAL_TIMEOUT,
    SIGNAL_BTN_L_CLICK,
    SIGNAL_BTN_U_CLICK,
    SIGNAL_BTN_D_CLICK,
    SIGNAL_BTN_R_CLICK,
    SIGNAL_BTN_L_LONG,
    SIGNAL_BTN_U_LONG,
    SIGNAL_BTN_D_LONG,
    SIGNAL_BTN_R_LONG
};

/*------------------------------------------------------- Function prototype: */

#endif /* MAIN_H_ */
