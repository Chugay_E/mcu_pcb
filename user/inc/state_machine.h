/**
 * @file		state_machine.h
 * @author		Chugay E.A.
 * @version		0.01
 * @date		03.05.2021
 * @brief		Функции автоматов состояния
 */

#ifndef STATE_MACHINE_H_
#define STATE_MACHINE_H_

/*----------------------------------------------------------------- Includes: */
#include "main.h"

/*-------------------------------------------------------------------- Macro: */

/*-------------------------------------------------------------------- Const: */
enum TopLevelState {
    USB_CONNECT = 0,
    USB_DISCONNECT,
    TOP_LEVEL_SELECT            //  просмотр списка главного меню
};

/*------------------------------------------------------ Function prototypes: */
/**
 * @brief   Основной автомат состояний. Навигация по списку Главного меню.
 *          Из данной функции вызываются остальные списки подменю.
 * @param   Сигнал нажатия кнопки, автоотключения и др.
 * @retval  Нет.
 */
void top_level_sm (uint8_t signal);

#endif /* STATE_MACHINE_H_ */
