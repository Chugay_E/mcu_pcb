/**
 * @file		file_gen.h
 * @author		Chugay E.A.
 * @version		
 * @date		
 * @brief		
 */

#ifndef FILE_GEN_H_
#define FILE_GEN_H_

/*----------------------------------------------------------------- Includes: */
#include "main.h"
#include <stdint.h>

/*-------------------------------------------------------------------- Macro: */
typedef struct {
    uint32_t sp_max;    //  максимальное значение скорости
    uint32_t sp_av;     //  среднее значение скорости
    uint32_t sp_min;    //  минимальное значение скорости
    uint32_t en_max;    //  максимальное значение энергии
    uint32_t en_av;     //  среднее значение энергии
    uint32_t en_min;    //  минимальное значение энергии
} COMMON_DATA;          //  24 байта

typedef struct {
    uint8_t sp_max;     //  номер записи максимальной скорости в массиве буфера
    uint8_t sp_min;     //  номер записи минимальной скорости в массиве буфера
    uint8_t en_max;     //  номер записи максимальной энергии в массиве буфера
    uint8_t en_min;     //  номер записи минимальной энергии в массиве буфера
} COMMON_POS;           //  4 байта

typedef struct {
    uint32_t energy;    //  энергия
    uint16_t speed;     //  скорость
    uint16_t interval;  //  время с предыдущего выстрела для расчета скорострельности, [ms].
    uint16_t mass;      //  масса
    uint16_t clbr;      //  калибр
} BULLIT_DATA;          //  12 байт

typedef struct {                        //  size in bytes:
    COMMON_DATA com;                    //  24
    COMMON_POS  pos;                    //  4
    BULLIT_DATA rec[80];                //  12 * 80 = 960
} BUFFER_DATA;                          //  total: 988

/*------------------------------------------------------- Function prototype: */
void create_test_data (void);
void mem_to_txt (uint8_t *buf, int block);

#endif /* FILE_GEN_H_ */
