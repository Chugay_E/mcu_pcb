/**
 * @file		main.c
 * @author		Chugay E.A.
 * @version		1.00
 * @date		06.09.2020
 * @brief       Тестовая прошивка платы mcu_pcb [rev1].
 * @details     STM32F103CBT6.
 *
 *              Функции инициализации:
 *              - системы тактирования;
 *              - системного таймера;
 *              - независимого сторожевого таймера;
 *
 *              Обработчики прерываний:
 *              - системы тактирования
 *              - системного таймера.
 */

/*----------------------------------------------------------------- Includes: */
#include "main.h"

/*-------------------------------------------------------------------- Macro: */

/*--------------------------------------------------------- Extern variables: */

/*--------------------------------------------------------- Public variables: */
STATUS_FLAG        status_flag;

/*-------------------------------------------------------- Private variables: */
static enum Signal         current_signal = SIGNAL_NONE;
static volatile uint32_t   sec_cnt = 0;
static volatile uint16_t   ms_cnt  = 0;
static BTN_SIGNAL btn;

/*---------------------------------------------- Private funcrion prototypes: */
void init_RCC (void);
void init_SysTick (void);
void init_IWDG (uint16_t wt);

/*----------------------------------------------------------------------------*/
/*--------------------------------------------------------------------- Main: */
/*----------------------------------------------------------------------------*/
int main (void)
{
    //  Основные модули системы:
    init_RCC();                             //	Настройка системы тактирования
    init_SysTick();                         //	Инициализация системного таймера SysTick
    init_IWDG(1000);                        //	Настройка WatchDog на время (msec)
    RCC->CR  |= (uint32_t)RCC_CR_CSSON;		//	Clock Security System
    RCC->CIR |= (uint32_t)RCC_CIR_HSERDYIE; //	Прерывание по готовности HSE
    __enable_irq();                         //  Разрешить прерывания глобально

    //  Инициализация перифирии:
    init_button_pin();                      //  Инициализация кнопок
    init_display();                         //  Инициализация дисплея

    init_usb_disconnect();
    MX_USB_DEVICE_Init();
    create_test_data();

    btn.value = 0x00;

/*--------------------------------------------------------------- Main while: */
    while(1) {
        IWDG_RESET;

        //  опрос кнопок:
        if (status_flag.btn_upd_en) {
            status_flag.btn_upd_en = 0;
            btn.value = button_debounce();
            //  Формирование сигнала для автомата состояний:
            if (btn.l_click) {
                current_signal = SIGNAL_BTN_L_CLICK;
            } else if (btn.u_click) {
                current_signal = SIGNAL_BTN_U_CLICK;
            } else if (btn.d_click) {
                current_signal = SIGNAL_BTN_D_CLICK;
            } else if (btn.r_click) {
                current_signal = SIGNAL_BTN_R_CLICK;
            } else if (btn.l_long) {
                current_signal = SIGNAL_BTN_L_LONG;
            } else if (btn.u_long) {
                current_signal = SIGNAL_BTN_U_LONG;
            } else if (btn.d_long) {
                current_signal = SIGNAL_BTN_D_LONG;
            } else if (btn.r_long) {
                current_signal = SIGNAL_BTN_R_LONG;
            }
        }
        if (btn.value == 0x00) current_signal = SIGNAL_NONE;
        btn.value = 0x00;

        //  Основной автомат:
        top_level_sm(current_signal);
    }
}



/*----------------------------------------------------------------------------*/
/*--------------------------------------------------- Initializatin function: */
/*----------------------------------------------------------------------------*/

void init_RCC (void)                            // 8 MHz HSE
{
    uint32_t StartUpCounter = 0;                // Счетчик циклов ожидания включения HSE

    RCC->CR &= (uint32_t)((uint32_t)~(RCC_CR_PLLON | RCC_CR_HSEON));
    RCC->CR |= ((uint32_t)RCC_CR_HSEON);        // Включение HSE
    do {                                        // Ждем готовности HSE в течении 500 циклов
        StartUpCounter++;
    } while (((RCC->CR & RCC_CR_HSERDY) == 0) && (StartUpCounter != 500));

    RCC->CFGR &= (uint32_t)((uint32_t)~(RCC_CFGR_SW | RCC_CFGR_HPRE | RCC_CFGR_PPRE1 |
                                        RCC_CFGR_PPRE2 |  RCC_CFGR_PLLSRC |
                                        RCC_CFGR_PLLXTPRE | RCC_CFGR_PLLMULL));
    // Конфигурация буфера FLASH
    FLASH->ACR |= FLASH_ACR_PRFTBE;
    FLASH->ACR &= (uint32_t)~FLASH_ACR_LATENCY;
    FLASH->ACR |= (uint32_t)FLASH_ACR_LATENCY_2;

    // Установка делителей частоты шин
    RCC->CFGR |= (uint32_t)RCC_CFGR_HPRE_DIV1;	// HCLK  = SYSCLK       (AHB)
    RCC->CFGR |= (uint32_t)RCC_CFGR_PPRE1_DIV2;	// PCLK1 = (HCLK / 2)   (APB1)
    RCC->CFGR |= (uint32_t)RCC_CFGR_PPRE2_DIV1;	// PCLK2 = HCLK         (APB2)

    if ((RCC->CR & RCC_CR_HSERDY) != 0) {
        RCC->CFGR |= (uint32_t)(RCC_CFGR_PLLSRC | RCC_CFGR_PLLMULL9);   //  72 MHz, HSE source
    } else {
        RCC->CFGR |= (uint32_t)(RCC_CFGR_PLLMULL9);                     //  36 MHz, HSI source
    }
    RCC->CR |= RCC_CR_PLLON;
    while((RCC->CR & RCC_CR_PLLRDY) == 0);

    RCC->CFGR &= (uint32_t)((uint32_t)~(RCC_CFGR_SW));
    RCC->CFGR |= (uint32_t)RCC_CFGR_SW_PLL;
    while ((RCC->CFGR & (uint32_t)RCC_CFGR_SWS) != (uint32_t)RCC_CFGR_SWS_PLL);
}



void init_SysTick (void)
{
    SysTick->LOAD = 71999;	// стартовое значение, загружаемое в SysTick->VAL (24 бита максимум!)
    SysTick->VAL  = 71999;	// очищаем регистр при запуске, очищаем бит COUNTFLAG регистра CTRL
    NVIC_SetPriority(SysTick_IRQn, 7);
    SysTick->CTRL =(SysTick_CTRL_CLKSOURCE_Msk |	// источник синхросигнала - HCLK
                    SysTick_CTRL_TICKINT_Msk |		// прерывание при достижении нуля
                    SysTick_CTRL_ENABLE_Msk);		// запуск таймера
}



void init_IWDG (uint16_t wt)	// wt от 7 до 26200 (6.4ms - 26.1952sec.)
{
    IWDG->KR = 0x5555;			// Ключ для доступа к регистрам таймера
    IWDG->PR = 7;				// Деление частоты 40КГц на 256
    IWDG->RLR = (wt * 40) >> 8;	// Запись значения регистра перезагрузки
    IWDG->KR = 0xAAAA;			// Перезагрузка
    IWDG->KR = 0xCCCC;			// Старт
}



/*----------------------------------------------------------------------------*/
/*------------------------------------------------------- Interrupt handlers: */
/*----------------------------------------------------------------------------*/

void NMI_Handler (void)
{
    if ((RCC->CIR & RCC_CIR_HSERDYF) || (RCC->CIR & RCC_CIR_CSSF)) {
        init_RCC();
        if (RCC->CIR & RCC_CIR_CSSF)
            RCC->CIR |= RCC_CIR_CSSC; // сброс флага прерывания
        else
            RCC->CIR |= RCC_CIR_HSERDYC; // сброс флага прерывания
    }
    while ((RCC->CIR & RCC_CIR_CSSF) || (RCC->CIR & RCC_CIR_HSERDYF));
}



void SysTick_Handler (void)
{
    if (ms_cnt < 999) {
        ms_cnt++;
    } else {
        ms_cnt = 0;
        sec_cnt++;
    }

    //  Опрос кнопок 50 раз в секунду:
    static uint8_t button_upd_cnt = 0;
    if (button_upd_cnt < 20) {
        button_upd_cnt++;
    } else {
        button_upd_cnt = 0;
        status_flag.btn_upd_en = 1;
    }

//  HAL_IncTick();
}
