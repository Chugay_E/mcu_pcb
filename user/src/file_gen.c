/**
 * @file		file_gen.c
 * @author		Chugay E.A.
 * @version		1.00
 * @date		10.09.2021
 * @brief
 */

/*----------------------------------------------------------------- Includes: */
#include "file_gen.h"

/*-------------------------------------------------------------------- Macro: */
#define FILE_VER        100

#define STR_FULL        64
#define STR_HALF        32

#define STR_COM_SP_VAL	22
#define STR_COM_SP_NO	28
#define STR_COM_EN_VAL	44
#define STR_COM_EN_NO	50
#define STR_COM_RPM_VAL	62

#define STR_MAIN_NUM	80	//	Количкство строк данных основной таблицы
#define STR_MAIN_NO		3	//	Позиция крайнего правого символа номера записи
#define STR_MAIN_SP		12	//	Позиция крайнего правого символа значения скорости
#define STR_MAIN_EN		24	//	Позиция крайнего правого символа значения энергии
#define STR_MAIN_RPM	34	//	Позиция крайнего правого символа значения скорострельности
#define STR_MAIN_INT	44	//	Позиция крайнего правого символа значения интервала от пред. измерения
#define STR_MAIN_CAL	53	//	Позиция крайнего правого символа значения калибра снаряда
#define STR_MAIN_MASS	62	//	Позиция крайнего правого символа значения массы снаряда

/*--------------------------------------------------------- Extern variables: */

/*--------------------------------------------------------- Public variables: */
BUFFER_DATA test_buf;
char name[16] = "Проба 26";

/*-------------------------------------------------------- Private variables: */
static const char str_reminder[]  = "! Please, for correct display of Cyrillic use UTF-8 codepage !\r\n";
static const char str_empty[]     = "                                                              \r\n";
static const char str_div_one[]   = "--------------------------------------------------------------\r\n";
static const char str_div_two[]   = "==============================================================\r\n";
static const char str_com_head[]  = "           |    Speed:   No: |       Energy:   No: |      RPM:\r\n";
static const char str_main_head[] = "No:   Speed:     Energy:      RPM:      Int:     Cal:    Mass:\r\n";
static const char *str_file[] = {
    [0] = "ver.:",		//	Версия файла, значение вида "Х.ХХ"
    [1] = "slot:",		//	Номер слота, значение вида "XX"
    [2] = "date:",		//	Дата измерений значение вида "ЧЧ.ММ.ГГГГ"
    [3] = "name:",		//	Имя записи, тестовая метка, записаная во FLASH
};

static const char *str_com[] = {
    [0] = "      Max:",
    [1] = "  Average:",
    [2] = "      Min:"
};

/*---------------------------------------------- Private funcrion prototypes: */

/*----------------------------------------------------------------- Function: */
void create_test_data (void)
{
    test_buf.com.sp_max = 50000;
    test_buf.com.sp_av  = 40000;
    test_buf.com.sp_min = 30000;
    test_buf.com.en_max = 150000;
    test_buf.com.en_av  = 140000;
    test_buf.com.en_min = 130000;

    test_buf.pos.sp_max = 12;
    test_buf.pos.sp_min = 13;
    test_buf.pos.en_max = 14;
    test_buf.pos.en_min = 15;

    uint8_t i;
    for (i = 0; i < 80; i++) {
        test_buf.rec[i].speed    = 1 + (i * 800);
        test_buf.rec[i].energy   = 2 + (i * 100000);
        test_buf.rec[i].mass     = 6 + (i * 100);
        test_buf.rec[i].clbr     = 5 + (i * 100);
        test_buf.rec[i].interval = 4 + (i * 100);
    }
}



void date_to_str (uint8_t *buf, uint8_t date, uint8_t month, uint8_t year)
{
    buf[(STR_FULL * 6) +  8] = (date / 10) + '0';
    buf[(STR_FULL * 6) +  9] = (date % 10) + '0';
    buf[(STR_FULL * 6) + 10] = '.';
    buf[(STR_FULL * 6) + 11] = (month / 10) + '0';
    buf[(STR_FULL * 6) + 12] = (month % 10) + '0';
    buf[(STR_FULL * 6) + 13] = '.';
    buf[(STR_FULL * 6) + 14] = '2';
    buf[(STR_FULL * 6) + 15] = '0';
    buf[(STR_FULL * 6) + 16] = (year / 10) + '0';
    buf[(STR_FULL * 6) + 17] = (year % 10) + '0';
}



void num_to_str (uint8_t *buf, uint16_t pos, uint32_t num, uint8_t dot, uint8_t width)
{
    if (dot > 10) dot = 10;
    pos--;

    uint8_t i;

    for (i = 0; i < dot; i++) {
        buf[pos - i] = (num % 10) + '0';
        num /= 10;
    }

    if (dot) {
        buf[pos - (i++)] = '.';
        width++;
    }

    if (num == 0) {
        buf[pos - (i++)] = '0';
    }

    for (; num > 0; i++) {
        buf[pos - i] = (num % 10) + '0';
        num /= 10;
    }

    if (width != 0) {
        for (; i < width; i++) {
            buf[pos - i] = '0';
        }
    }
}



void mem_to_txt (uint8_t *buf, int block)
{
//	if (sizeof(buf) != 512) return;
    static uint8_t str_cnt = 0;		//	счетчик для формирования строк основной таблицы
    static uint8_t div_cnt = 0;		//	счетчик для позиции разделителя в поле основной таблицы

    switch (block) {
    case 0:		//	File information
        str_cnt = 0;
        div_cnt = 0;
        for (uint8_t i = 0; i < STR_FULL; i++) buf[(STR_FULL * 0) + i] = str_div_two[i];
        for (uint8_t i = 0; i < STR_FULL; i++) buf[(STR_FULL * 1) + i] = str_reminder[i];
        for (uint8_t i = 0; i < STR_FULL; i++) buf[(STR_FULL * 2) + i] = str_div_two[i];
        for (uint8_t j = 0; j < 10; j++) {
            for (uint8_t i = 0; i < STR_HALF; i++) {
                buf[192 + (j << 5) + i] = str_empty[STR_HALF + i];
            }
        }
        for (uint8_t j = 0; j < 4; j++) {
            for (uint8_t i = 0; i < 5; i++) {
                buf[320 + (j << 5) + i] = str_file[j][i];
            }
        }
        num_to_str(buf, ((STR_HALF * 10) + 12), FILE_VER, 2, 0);			//	Версия файла
        num_to_str(buf, ((STR_HALF * 11) + 10),        3, 0, 2);			//	Номер слота
        date_to_str(buf, 20, 1, 22);										//	Дата
        for (uint8_t i = 0; name[i] != '\0'; i++) buf[424 + i] = name[i];	//	Имя файла
        break;
    case 1:		//	Common measure information
        for (uint8_t i = 0; i < STR_FULL; i++) buf[(STR_FULL * 0) + i] = str_div_two[i];
        for (uint8_t i = 0; i < STR_FULL; i++) buf[(STR_FULL * 1) + i] = str_com_head[i];
        for (uint8_t i = 0; i < STR_FULL; i++) buf[(STR_FULL * 2) + i] = str_div_one[i];
        buf[139] = '|';
        buf[157] = '|';
        buf[179] = '|';
        for (uint8_t j = 0; j < 3; j++) {
            for (uint8_t i = 0; i < 10; i++) {
                buf[192 + (j << 6) + i] = str_com[j][i];
            }
            for (uint8_t i = 10; i < STR_FULL; i++) {
                buf[192 + (j << 6) + i] = str_empty[i];
            }
            buf[203 + (j << 6)] = '|';
            buf[221 + (j << 6)] = '|';
            buf[243 + (j << 6)] = '|';
        }
        for (uint8_t i = 0; i < STR_FULL; i++) buf[384 + i] = str_div_two[i];
        for (uint8_t i = 0; i < STR_FULL; i++) buf[448 + i] = str_empty[i];

        //	Common Speed:
        num_to_str(buf, ((STR_FULL * 3) + STR_COM_SP_VAL), test_buf.com.sp_max, 1, 0);
        num_to_str(buf, ((STR_FULL * 3) + STR_COM_SP_NO),  test_buf.pos.sp_max, 0, 0);
        num_to_str(buf, ((STR_FULL * 4) + STR_COM_SP_VAL), test_buf.com.sp_av,  1, 0);
        num_to_str(buf, ((STR_FULL * 5) + STR_COM_SP_VAL), test_buf.com.sp_min, 1, 0);
        num_to_str(buf, ((STR_FULL * 5) + STR_COM_SP_NO),  test_buf.pos.sp_min, 0, 0);

        //	Common Energy:
        num_to_str(buf, ((STR_FULL * 3) + STR_COM_EN_VAL), test_buf.com.en_max, 2, 0);
        num_to_str(buf, ((STR_FULL * 3) + STR_COM_EN_NO),  test_buf.pos.en_max, 0, 0);
        num_to_str(buf, ((STR_FULL * 4) + STR_COM_EN_VAL), test_buf.com.en_av,  2, 0);
        num_to_str(buf, ((STR_FULL * 5) + STR_COM_EN_VAL), test_buf.com.en_min, 2, 0);
        num_to_str(buf, ((STR_FULL * 5) + STR_COM_EN_NO),  test_buf.pos.en_min, 0, 0);

        //	Common RPM:
        num_to_str(buf, ((STR_FULL * 3) + STR_COM_RPM_VAL), 0, 2, 0);
        num_to_str(buf, ((STR_FULL * 4) + STR_COM_RPM_VAL), 0, 2, 0);
        num_to_str(buf, ((STR_FULL * 5) + STR_COM_RPM_VAL), 0, 2, 0);
        break;
    case 2:		//	Main table head + first 5 records:
        for (uint8_t i = 0; i < STR_FULL; i++) buf[(STR_FULL * 0) + i] = str_div_two[i];
        for (uint8_t i = 0; i < STR_FULL; i++) buf[(STR_FULL * 1) + i] = str_main_head[i];
        for (uint8_t i = 0; i < STR_FULL; i++) buf[(STR_FULL * 2) + i] = str_div_one[i];

        for (uint8_t j = 0; j < 5; j++) {
            for (uint8_t i = 0; i < STR_FULL; i++) buf[STR_FULL * (j + 3) + i] = str_empty[i];
            num_to_str(buf, ((STR_FULL * (j + 3)) + STR_MAIN_NO),   (uint32_t) (str_cnt + 1),                  0, 3);
            num_to_str(buf, ((STR_FULL * (j + 3)) + STR_MAIN_SP),   (uint32_t) test_buf.rec[str_cnt].speed,    1, 0);
            num_to_str(buf, ((STR_FULL * (j + 3)) + STR_MAIN_EN),   (uint32_t) test_buf.rec[str_cnt].energy,   2, 0);
            num_to_str(buf, ((STR_FULL * (j + 3)) + STR_MAIN_RPM),  (uint32_t) 99999,                          2, 0);
            num_to_str(buf, ((STR_FULL * (j + 3)) + STR_MAIN_INT),  (uint32_t) test_buf.rec[str_cnt].interval, 3, 0);
            num_to_str(buf, ((STR_FULL * (j + 3)) + STR_MAIN_CAL),  (uint32_t) test_buf.rec[str_cnt].clbr,     2, 0);
            num_to_str(buf, ((STR_FULL * (j + 3)) + STR_MAIN_MASS), (uint32_t) test_buf.rec[str_cnt].mass,     2, 0);
            str_cnt++;
            div_cnt++;
        }
        div_cnt++;
        break;
    default:		//	Main table
        for (uint8_t j = 0; j < 8; j++) {
            if (str_cnt < STR_MAIN_NUM) {
                if ((div_cnt % 11) == 0) {
                    div_cnt++;
                    for (uint8_t i = 0; i < STR_FULL; i++) buf[(STR_FULL * j) + i] = str_div_one[i];
                    if ((++j) >= 8) return;
                }
                for (uint8_t i = 0; i < STR_FULL; i++) buf[(STR_FULL * j) + i] = str_empty[i];
                num_to_str(buf, ((STR_FULL * j) + STR_MAIN_NO),   (uint32_t) (str_cnt + 1),                  0, 3);
                num_to_str(buf, ((STR_FULL * j) + STR_MAIN_SP),   (uint32_t) test_buf.rec[str_cnt].speed,    1, 0);
                num_to_str(buf, ((STR_FULL * j) + STR_MAIN_EN),   (uint32_t) test_buf.rec[str_cnt].energy,   2, 0);
                num_to_str(buf, ((STR_FULL * j) + STR_MAIN_RPM),  (uint32_t) 88888,                          2, 0);
                num_to_str(buf, ((STR_FULL * j) + STR_MAIN_INT),  (uint32_t) test_buf.rec[str_cnt].interval, 3, 0);
                num_to_str(buf, ((STR_FULL * j) + STR_MAIN_CAL),  (uint32_t) test_buf.rec[str_cnt].clbr,     2, 0);
                num_to_str(buf, ((STR_FULL * j) + STR_MAIN_MASS), (uint32_t) test_buf.rec[str_cnt].mass,     2, 0);
                str_cnt++;
                div_cnt++;
            } else if (str_cnt == STR_MAIN_NUM){
                for (uint8_t i = 0; i < STR_FULL; i++) buf[(STR_FULL * j) + i] = str_div_two[i];
                str_cnt++;
            } else {
                for (uint8_t i = 0; i < STR_FULL; i++) buf[(STR_FULL * j) + i] = str_empty[i];
            }
        }
        break;
    }
}
