/**
 * @file		state_machine.c
 * @author		Chugay E.A.
 * @version		0.01
 * @date		03.05.2021
 * @brief		Функции автоматов состояния
 *              *_sm - state machine
 *              *_cs - current state
 */

/*----------------------------------------------------------------- Includes: */
#include "state_machine.h"

/*-------------------------------------------------------------------- Macro: */

/*--------------------------------------------------------- Extern variables: */

/*--------------------------------------------------------- Public variables: */

/*-------------------------------------------------------- Private variables: */
static enum TopLevelState top_level_cs = TOP_LEVEL_SELECT;

/*---------------------------------------------- Private funcrion prototypes: */

/*----------------------------------------------------------------- Function: */

/*----------------------------------------------------- Верхний уровень меню: */
void top_level_sm (uint8_t signal)
{
    static uint8_t tl_tmp = 0;

    switch (top_level_cs) {
    case TOP_LEVEL_SELECT:
        switch (signal) {
        case SIGNAL_BTN_U_CLICK: case SIGNAL_BTN_U_LONG:
            (tl_tmp > 0) ? (tl_tmp--) : (tl_tmp = (TOP_LEVEL_SELECT - 1));
            break;
        case SIGNAL_BTN_D_CLICK: case SIGNAL_BTN_D_LONG:
            (tl_tmp < (TOP_LEVEL_SELECT - 1)) ? (tl_tmp++) : (tl_tmp = 0);
            break;
        case SIGNAL_BTN_L_CLICK:
            tl_tmp = 0;
            break;
        case SIGNAL_BTN_R_CLICK:
            top_level_cs = tl_tmp;
            tl_tmp = 0;
            break;
        }
        send_frame_to_display(tl_tmp, top_level_menu_screen);
        break;
    case USB_CONNECT:
        USB_CTRL_CONNECT;
        top_level_cs = TOP_LEVEL_SELECT;    //  Подключение USB
        tl_tmp = USB_DISCONNECT;
        break;
    case USB_DISCONNECT:
        USB_CTRL_DISCONNECT;
        top_level_cs = TOP_LEVEL_SELECT;    //  Отключение USB
        tl_tmp = USB_CONNECT;
        break;
    }
}
