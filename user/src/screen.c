/**
 * @file		screen.c
 * @author		Chugay E.A.
 * @version		0.01
 * @date		03.05.2021
 * @brief		Функции отрисовки экрана с использованием графических примитивов
 * File used UTF-8 code page. Don't convert other codepage for correct work.
 */

/*----------------------------------------------------------------- Includes: */
#include "screen.h"

/*-------------------------------------------------------------------- Macro: */
#define FONT_16_PIX     FONT_H16
#define FONT_08_PIX     FONT_NORMAL
#define MENU_STR_H      WIDGET_STR_H16

#define DISPLAY_STR_0   (uint8_t)56
#define DISPLAY_STR_1   (uint8_t)48
#define DISPLAY_STR_2   (uint8_t)40
#define DISPLAY_STR_3   (uint8_t)32
#define DISPLAY_STR_4   (uint8_t)24
#define DISPLAY_STR_5   (uint8_t)16
#define DISPLAY_STR_6   (uint8_t)8
#define DISPLAY_STR_7   (uint8_t)0

/*--------------------------------------------------------- Extern variables: */

/*--------------------------------------------------------- Public variables: */

/*---------------------------------------------- Private funcrion prototypes: */

/*-------------------------------------------------------- Private variables: */
static const char *top_level_list[] = {     //  список меню "ГЛАВНОЕ МЕНЮ"
    [USB_CONNECT]    = "CONNECT",
    [USB_DISCONNECT] = "DISCONNECT"
};

/*----------------------------------------------------------------- Function: */

/*----------------------------------------------------- Верхний уровень меню: */
void top_level_menu_screen (uint8_t signal)
{
    //  отображение строк списка меню:
    frame_font_select(FONT_16_PIX);
    widget_menu_list(top_level_list, (TOP_LEVEL_SELECT - 1), MENU_STR_H, signal);

    //  отображение кнопок в нижней строке:
    frame_font_select(FONT_08_PIX);
    widget_btn_l("BЫXOД", BTN_L);
    widget_btn_up(0x1E, BTN_U);
    widget_btn_down(0x1F, BTN_D);
    widget_btn_r("BЫБOP", BTN_R);

    //  DEBUG:
    frame_text_string(0, 56, "VERSION 0.0.1");
}
