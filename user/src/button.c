/**
 * @file		button.c
 * @author		Chugay E.A.
 * @version		0.02
 * @date		03.05.2021
 * @brief		Функции инициализации входов GPIO и обработки нажатия кнопок.
 * @details     - в версии 0.02 удалена глобальная переменная для хранения
 *                сигнала кнопок, это значение возвращается функцией button_debounce().
 */

/*----------------------------------------------------------------- Includes: */
#include "button.h"

/*-------------------------------------------------------------------- Macro: */
#define BTN_QUANTITY	((uint8_t)4)
#define DEBOUNCE_TIME	((uint8_t)2)
#define BTN_LONG		((uint8_t)50)

/*---------------------------------------------------------- Extern variable: */

/*--------------------------------------------------------- Public variables: */

/*--------------------------------------------------------- Private variable: */
static uint8_t btn_cnt[BTN_QUANTITY] = {0, 0, 0, 0};

/*----------------------------------------------------------------- Function: */
//	Init buttons GPIO
void init_button_pin (void)
{
    RCC->APB2ENR |= RCC_APB2ENR_IOPAEN;
    //	PA0, PA1, PA2, PA3, BTN
    GPIOA->CRL &= ~(GPIO_CRL_CNF0 | GPIO_CRL_CNF1 | GPIO_CRL_CNF2 | GPIO_CRL_CNF3);
    GPIOA->CRL |= (GPIO_CRL_CNF0_1 | GPIO_CRL_CNF1_1 | GPIO_CRL_CNF2_1 | GPIO_CRL_CNF3_1);
}



//	Button handler
uint8_t button_debounce (void)
{
    static volatile uint8_t btn_flag = 0;

    uint8_t input = 0x00;
    if (BTN_L) input |= (1 << 0);
    if (BTN_U) input |= (1 << 1);
    if (BTN_D) input |= (1 << 2);
    if (BTN_R) input |= (1 << 3);

    BTN_SIGNAL btn;
    btn.value = 0x00;

    for(uint8_t i=0; i < BTN_QUANTITY; i++) {
        if ((input & (1 << i)) && !(btn_flag & (1 << i))) {
            btn_flag |= (1 << i);
        } else if ((input & (1 << i)) && (btn_flag & (1 << i))) {
            if (btn_cnt[i] <= BTN_LONG) {
                btn_cnt[i]++;
            }
            if (btn_cnt[i] == BTN_LONG) {
                btn.value |= (1 << (i + BTN_QUANTITY));
                btn_cnt[i] = (BTN_LONG - (BTN_LONG >> 3));
            } else {
                btn.value &= ~(1 << (i + BTN_QUANTITY));
            }
            (btn_cnt[i] == DEBOUNCE_TIME) ? (btn.value |= (1 << i)) : (btn.value &= ~(1 << i));
        } else if (!(input & (1 << i)) && (btn_flag & (1 << i))) {
            btn.value &= ~((1 << i) | (1 << (i + BTN_QUANTITY)));
            btn_flag &= ~(1 << i);
            btn_cnt[i] = 0x00;
        }
    }

    return btn.value;
}
