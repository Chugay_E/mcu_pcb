# Base template for PCB "mcu_pcb_rev1".

**STM32F103CBT6.**\
**CMSIS 4.2.0**.\
[CMake](https://cmake.org/)\
[GCC arm-none-eabi-*** toolchain](https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm/downloads).

---

## Initialisation:
- RCC;
- SysTick for 1 ms. interrupt;
- Watchdog, 1 sec.;
- Input for button (PA0...PA3);
- Display SPI1, DMA and control pins.

---

## MCU pinout:
| PIN | PORT | GPIO |                        FUNCTION                  |
|----:|:----:|:----:|:-------------------------------------------------|
| 10  | PA0  | IN   | Button SB2                                       |
| 11  | PA1  | IN   | Button SB3                                       |
| 12  | PA2  | IN   | Button SB4                                       |
| 13  | PA3  | IN   | Button SB5                                       |
| 14  | PA4  | OUT  | Display, DC                                      |
| 15  | PA5  | SPI1 | Display, SCK                                     |
| 16  | PA6  | OUT  | Display, RESET                                   |
| 17  | PA7  | SPI1 | Display, SDA                                     |
| 29  | PA8  |      | - Reserve (XP2:10)                               |
| 30  | PA9  |      | - Reserve (XP2:9)                                |
| 31  | PA10 |      | - Reserve (USB disconnect)                       |
| 32  | PA11 |      | - Reserve (USB D-)                               |
| 33  | PA12 |      | - Reserve (USB D+)                               |
| 34  | PA13 | SWD  | SWD, DIO                                         |
| 37  | PA14 | SWD  | SWD, CLK                                         |
| 38  | PA15 |      | - Reserve (XP2:16)                               |
| 18  | PB0  | OUT  | Display, CE                                      |
| 19  | PB1  |      | - Reserve (XP1:10)                               |
| 20  | PB2  |      | - Reserve (XP1:9)                                |
| 39  | PB3  |      | - Reserve (XP2:15)                               |
| 40  | PB4  |      | - Reserve (XP2:18)                               |
| 41  | PB5  |      | - Reserve (XP2:17)                               |
| 42  | PB6  |      | - Reserve (XP2:20)                               |
| 43  | PB7  |      | - Reserve (XP2:19)                               |
| 45  | PB8  |      | - Reserve (XP1:16)                               |
| 46  | PB9  |      | - Reserve (XP1:13)                               |
| 21  | PB10 |      | - Reserve (XP1:8) (UART3, TX)                    |
| 22  | PB11 |      | - Reserve (XP1:6) (UART3, RX)                    |
| 25  | PB12 |      | - Reserve (XP2:6)                                |
| 26  | PB13 |      | - Reserve (XP2:5)                                |
| 27  | PB14 |      | - Reserve (XP2:8)                                |
| 28  | PB15 |      | - Reserve (XP2:7)                                |
|  2  | PC13 |      | - Reserve (XP1:14)                               |
|  3  | PC14 |      | - Reserve (XP1:11)                               |
|  4  | PC15 |      | - Reserve (XP1:12)                               |
