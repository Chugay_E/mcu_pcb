/**
  * @file    usbd_msc_data.c
  * @author  MCD Application Team
  * @brief   This file provides all the vital inquiry pages and sense data.
  */

/* Includes ------------------------------------------------------------------*/
#include "usbd_msc_data.h"



/* USB Mass storage Page 0 Inquiry Data */
const uint8_t  MSC_Page00_Inquiry_Data[] =
{
    0x00,
    0x00,
    0x00,
    (LENGTH_INQUIRY_PAGE00 - 4U),
    0x00,
    0x80,
    0x83
};



/* USB Mass storage sense 6  Data */
const uint8_t  MSC_Mode_Sense6_data[] =
{
    0x00,
    0x00,
    0x00,
    0x00,
    0x00,
    0x00,
    0x00,
    0x00
};



/* USB Mass storage sense 10  Data */
const uint8_t  MSC_Mode_Sense10_data[] =
{
    0x00,
    0x06,
    0x00,
    0x00,
    0x00,
    0x00,
    0x00,
    0x00
};

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
