/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file    stm32f1xx_it.c
  * @brief   Interrupt Service Routines.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f1xx_it.h"

/* Private typedef -----------------------------------------------------------*/

/* Private define ------------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/

/* External variables --------------------------------------------------------*/
extern PCD_HandleTypeDef hpcd_USB_FS;



/******************************************************************************/
/*           Cortex-M3 Processor Interruption and Exception Handlers          */
/******************************************************************************/
/**
 * @brief   This function handles Hard fault interrupt.
 */
void HardFault_Handler(void)
{
    while (1);
}



/**
 * @brief   This function handles Memory management fault.
 */
void MemManage_Handler(void)
{
    while (1);
}



/**
 * @brief This function handles Prefetch fault, memory access fault.
 */
void BusFault_Handler(void)
{
    while (1);
}



/**
 * @brief   This function handles Undefined instruction or illegal state.
 */
void UsageFault_Handler(void)
{
    while (1);
}



/**
 * @brief   This function handles System service call via SWI instruction.
 */
void SVC_Handler(void)
{

}



/**
 * @brief   This function handles Debug monitor.
 */
void DebugMon_Handler(void)
{

}



/**
 * @brief   This function handles Pendable request for system service.
 */
void PendSV_Handler(void)
{

}



/******************************************************************************/
/*               Peripherial Interruption and Exception Handlers              */
/******************************************************************************/
/**
 * @brief   This function handles USB high priority or CAN TX interrupts.
 */
void USB_HP_CAN1_TX_IRQHandler(void)
{
    HAL_PCD_IRQHandler(&hpcd_USB_FS);
}



/**
 * @brief   This function handles USB low priority or CAN RX0 interrupts.
 */
void USB_LP_CAN1_RX0_IRQHandler(void)
{
    HAL_PCD_IRQHandler(&hpcd_USB_FS);
}

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
