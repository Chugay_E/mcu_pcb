/**
  * @file           : usbd_storage_if.h
  * @version        : v2.0_Cube
  * @brief          : Header for usbd_storage_if.c file.
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __USBD_STORAGE_IF_H__
#define __USBD_STORAGE_IF_H__

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "usbd_msc.h"

/** STORAGE Interface callback. */
extern USBD_StorageTypeDef USBD_Storage_Interface_fops_FS;

#ifdef __cplusplus
}
#endif

#endif /* __USBD_STORAGE_IF_H__ */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
